#!/bin/bash

echo "Opções disponíveis:"
echo "1 para Mostrar data e hora no prompt"
echo "2 para Mostrar nome do usuário no prompt"
echo "3 para Mostrar diretório atual no prompt"
echo "4 para Mostrar data e hora, nome do usuário e diretório atual no prompt"
echo "5 para Mudar a cor do texto do prompt para vermelho"
echo "6 para Voltar para o prompt original"

read -p "Escolha uma opção: " opcao

((opcao==1)) && export PS1="\d \t $ "
((opcao==2)) && export PS1="\u $ "
((opcao==3)) && export PS1="\w $ "
((opcao==4)) && export PS1="\d \t \u \w $ "
((opcao==5)) && export PS1='\[\e[31m\]\u@\h:\w \$\[\e[m\] '
((opcao==6)) && unset PS1 && echo "Prompt original restaurado."
