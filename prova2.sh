#!/bin/bash

dia1=$(date -d "next Wednesday" +%d-%m-%Y) && mkdir backup_$(date -d "this wednesday" +%d-%m-%Y)
dia2=$(date -d "next Wednesday +1 week" +%d-%m-%Y) && mkdir backup_$(date -d "next Wednesday +1 week" +%d-%m-%Y)
dia3=$(date -d "next Wednesday +2 weeks" +%d-%m-%Y) && mkdir backup_$(date -d "next Wednesday +2 weeks" +%d-%m-%Y)
dia4=$(date -d "next Wednesday +3 weeks" +%d-%m-%Y) && mkdir backup_$(date -d "next Wednesday +3 weeks" +%d-%m-%Y)

echo "Proxima quarta-feira: $dia1"
echo "Proxima quarta-feira essa a outra: $dia2"
echo "Proxima quarta-feira essa a outra da outra: $dia3"
echo "Proxima quarta-feira essa a outra da outra da outra: $dia4"
echo "Pastas geradas abaixo: "
ls
