#!/bin/bash


echo "Digite a operação que deseja realizar:"
echo "+ para Somar"
echo "- para Subtrair"
echo "* para Multiplicar"
echo "/ para Dividir"
echo "sqrt(Número) para Raiz quadrada"
echo "Exemplo 5 + 5 para soma (Digitar com espaço entre o operador que escolher"
echo "sqrt(Número 5) para raiz quadrada de 5"
echo ""
read -p "Digite Operando 1, operador, e se for o caso o Operando 2: " num1 op num2 <&0
echo "$num1 $op $num2" | bc -l
