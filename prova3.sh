#!/bin/bash
arquivo1=$1
read -p "Digite o nome do arquivo que deseja pesquisar: " arquivo1
[ -e "$arquivo1" ] && echo "$arquivo1 foi localizado no diretório atual." && exit
[ -e "/tmp/$arquivo1" ] && echo "$arquivo1 foi localizado em /tmp." && exit
[ -e "/etc/$arquivo1" ] && echo "$arquivo1 foi localizado em /etc." && exit
echo "$arquivo1 não localizado."

